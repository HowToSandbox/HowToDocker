#!/bin/bash

set -e

SELF="`readlink $0 2>/dev/null || echo $0`"
SELF="`dirname $SELF`"
SELF="`cd $SELF; pwd`"

IMAGE=demo

if test -z "$HOME"
then
    echo "ERROR: HOME is not set"
    exit 1
fi

# Uncomment this (or set it in environment) if you need really fresh stuff
# BUILD_OPTS="--no-cache"

if [ $UID -eq 0 ]; then
    echo
    echo "Error: do not run this script as root!"
    echo
    exit 1
fi

if test "$(uname -s)" = "Darwin"
then
    eval "$(docker-machine env default)"
elif ! id | grep -q docker
then
    echo
    echo "Please add yourself to the docker group, e.g."
    echo
    echo "sudo usermod -a -G docker $LOGNAME"
    echo
    echo "and then log out and back in."
    echo
    exit 3
fi

echo "Preparing ssh keys and other resources..."
rm -rf $SELF/resources/.ssh
mkdir -p $SELF/resources/.ssh
test -d $HOME/.ssh && cp -r $HOME/.ssh $SELF/resources/
cp $SELF/../resources/id_rsa* $SELF/resources/.ssh/
cp $SELF/resources/.ssh/id_rsa.pub $SELF/resources/.ssh/authorized_keys
chmod 700 $SELF/resources/.ssh && chmod 400 $SELF/resources/.ssh/*
cd $SELF/resources/
cd $OLDPWD

echo "Building $IMAGE image ..."
echo
docker build \
       $BUILD_OPTS \
       --build-arg USER=$(id -un) \
       --build-arg UID=$(id -u) \
       --build-arg HOME="$HOME" \
       --tag=$IMAGE $* $SELF

# also copy keys to
echo "copying ssh keys to $HOME/.ssh"
cp $SELF/../resources/id_rsa $HOME/.ssh/

echo $0 done.
