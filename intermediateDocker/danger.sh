#
# DANGER: Read the whole script before you run it.
# DANGER: It does dangerous things
#
docker rm -f demo
docker rmi demo
docker rmi $(docker images -a -q -f dangling=true)

./build.sh
echo "---------"
echo "Done Building"
echo "---------"
./run.sh